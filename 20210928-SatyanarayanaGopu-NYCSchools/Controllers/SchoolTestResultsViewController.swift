//
//  SchoolTestResultsViewController.swift
//  20210928-SatyanarayanaGopu-NYCSchools
//
//  Created by Satya on 9/28/21.
//

import UIKit

class SchoolTestResultsViewController: UIViewController {
    
    @IBOutlet private weak var schoolNameLabel: UILabel!
    
    @IBOutlet private weak var containerStackView: UIStackView!
    @IBOutlet private weak var testTakerCountLabel: UILabel!
    @IBOutlet private weak var mathAverageLabel: UILabel!
    @IBOutlet private weak var readingAverageLabel: UILabel!
    @IBOutlet private weak var writingAverageLabel: UILabel!
    @IBOutlet private weak var noDataErrorLabel: UILabel!
    
    var schoolId: String!
    var dataManager: DataManager!
    
    static func createViewController(schoolId: String, dataManager: DataManager) -> SchoolTestResultsViewController {
        let storyBoard = UIStoryboard(name: "SchoolTestResultsViewController", bundle: nil)
        let testResultsVC = storyBoard.instantiateViewController(identifier: "SchoolTestResultsViewController") as! SchoolTestResultsViewController
        testResultsVC.schoolId = schoolId
        testResultsVC.dataManager = dataManager
        return testResultsVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        updateViews()
    }
    
    private func setupViews() {
        self.title = "SAT Results"
        view.backgroundColor = .white
    }
    
    /*
    Show the school name label if a valid entry is availbale in either schools or testResults database.
    Show the data container view if testResults are available for the given schoolId.
    Hide the data container and show an error label when there is no record for the given schoolId.
     
    Some test results contain an 's' instead of actual data for their fields. Not considering it as a
     special case and showing it as it is for the user.
    */
    private func updateViews() {
        if let schoolSATResult = dataManager.testResultsOfSchool(with: schoolId) {
            schoolNameLabel.text = schoolSATResult.schoolName
            testTakerCountLabel.text = schoolSATResult.testTakerCount
            mathAverageLabel.text = schoolSATResult.mathAvg
            readingAverageLabel.text = schoolSATResult.readingAvg
            writingAverageLabel.text = schoolSATResult.writingAvg
            noDataErrorLabel.isHidden = true
            return
        } else if let school = dataManager.school(with: schoolId) {
            schoolNameLabel.text = school.name
        } else {
            schoolNameLabel.isHidden = true
        }
        noDataErrorLabel.isHidden = false
        containerStackView.isHidden = true
    }

}
