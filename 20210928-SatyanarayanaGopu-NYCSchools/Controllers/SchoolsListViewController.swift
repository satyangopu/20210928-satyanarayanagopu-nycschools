//
//  SchoolsListViewController.swift
//  20210928-SatyanarayanaGopu-NYCSchools
//
//  Created by Satya on 9/28/21.
//

import UIKit

class SchoolsListViewController: UITableViewController {
    private let cellReuseID = "cell"
    private var dataManager: DataManager
    // Used to present alerts or loading indicators.
    private var utilityViewPresenter: UtilityViewPresenter
    
    private var schools: [School] = []
    
    init(dataManager: DataManager) {
        self.dataManager = dataManager
        self.utilityViewPresenter = ViewPresenter()
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        fetchData()
    }
    
    private func setupViews() {
        self.title = "NYC Hight Schools"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseID)
    }
    
    private func fetchData() {
        dataManager.observeSchools (observer: {[weak self] result in
            DispatchQueue.main.async {
                guard let strongSelf = self else { return }
                strongSelf.utilityViewPresenter.presentLoadingIndicator(on: strongSelf)
                switch result {
                case .success(let schools):
                    strongSelf.schools = schools
                    strongSelf.tableView.reloadData()
                    strongSelf.utilityViewPresenter.dismissLoadingIndicator(from: strongSelf)
                case .failure(let error):
                    strongSelf.utilityViewPresenter.dismissLoadingIndicator(from: strongSelf)
                    strongSelf.utilityViewPresenter.presentAlert(on: strongSelf, title: "Error", message: error.localizedDescription)
                }
            }
        })
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseID, for: indexPath)
        tableViewCell.textLabel?.numberOfLines = 0
        tableViewCell.textLabel?.text = schools[indexPath.row].name
        tableViewCell.accessoryType = .disclosureIndicator
        return tableViewCell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schoolId = schools[indexPath.row].id
        let testResultsVC = SchoolTestResultsViewController.createViewController(schoolId: schoolId, dataManager: dataManager)
        navigationController?.pushViewController(testResultsVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
