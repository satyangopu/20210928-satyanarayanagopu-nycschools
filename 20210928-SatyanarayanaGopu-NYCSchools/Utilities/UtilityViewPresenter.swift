//
//  UtilityViewPresenter.swift
//  20210928-SatyanarayanaGopu-NYCSchools
//
//  Created by Satya on 9/28/21.
//

import UIKit

protocol AlertPresnter {
    func presentAlert(on vc: UIViewController, title: String, message: String?)
}

protocol LoadingIndicatorPresenter {
    func presentLoadingIndicator(on vc: UIViewController)
    func dismissLoadingIndicator(from vc: UIViewController)
}

protocol UtilityViewPresenter: AlertPresnter & LoadingIndicatorPresenter {}


struct ViewPresenter: UtilityViewPresenter {
    
    func presentAlert(on vc: UIViewController, title: String, message: String? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        weak var weakAlert = alertController
        let defaultAction = UIAlertAction(title: "Ok", style: .cancel) { (action) in
            weakAlert?.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(defaultAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    // Make sure not to present the activity twice
    func presentLoadingIndicator(on vc: UIViewController) {
        for view in vc.view.subviews {
            if view is UIActivityIndicatorView {
                return
            }
        }
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        vc.view.addSubview(activityIndicator)
        activityIndicator.color = .darkGray
        activityIndicator.center = vc.view.center
        activityIndicator.startAnimating()
    }
    
    func dismissLoadingIndicator(from vc: UIViewController) {
        for view in vc.view.subviews {
            if view is UIActivityIndicatorView {
                view.removeFromSuperview()
                return
            }
        }
    }
    
}
