//
//  DataManager.swift
//  20210928-SatyanarayanaGopu-NYCSchools
//
//  Created by Satya on 9/28/21.
//

import Foundation

// Type responsible for handling of the application data.
protocol DataManager {
    typealias SchoolDataObserver = (Result<[School], Error>) -> ()
    
    func observeSchools(observer: @escaping SchoolDataObserver)
    // Since the data being downloaded is for 2 different years, test results may not be availble for an entry in the school list.
    func testResultsOfSchool(with id: String) -> SchoolSATResult?
    func school(with id: String) -> School?
}

class DefaultDataManager {
    
    private var dataProvider: DataProvider
    private var schools = [School]()
    private var testResults = [SchoolSATResult]()
    
    private var schoolDataObserver: SchoolDataObserver?
    
    init(dataProvider: DataProvider) {
        self.dataProvider = dataProvider
        
        retrieveData()
    }
    
    private func retrieveData() {
        // TODO: Implement an in app storage to facilitate offline usage.
        dataProvider.fetchSchools {[weak self] result in
            switch result {
            case .success(let schools):
                self?.schools = schools
                self?.schoolDataObserver?(.success(schools))
            case .failure(let error):
                self?.schoolDataObserver?(.failure(error))
            }
        }
        
        dataProvider.fetchSchoolSATResult {[weak self] result in
            switch result {
            case .success(let schoolTestResults):
                self?.testResults = schoolTestResults
            case .failure(_):
                self?.testResults = []
            }
        }
        
    }
    
}

extension DefaultDataManager: DataManager {
    
    func observeSchools(observer: @escaping SchoolDataObserver) {
        self.schoolDataObserver = observer
        observer(.success(schools))
    }
    
    func testResultsOfSchool(with id: String) -> SchoolSATResult? {
        for testResult in testResults {
            if testResult.id == id {
                return testResult
            }
        }
        return nil
    }
    
    func school(with id: String) -> School? {
        for school in schools {
            if school.id == id {
                return school
            }
        }
        return nil
    }
    
}
