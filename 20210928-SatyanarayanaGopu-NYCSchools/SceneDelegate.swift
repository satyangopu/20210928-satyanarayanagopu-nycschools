//
//  SceneDelegate.swift
//  20210928-SatyanarayanaGopu-NYCSchools
//
//  Created by Satya on 9/28/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    // App is not guaranteed to work fine in the dark mode. Please use light mode to test it.
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        // Update the NetworkinigClient with a valid authentication token to use it.
        let dataProvider = FileBasedDataProvider() //NetworkingClient()
        let dataManager: DataManager = DefaultDataManager(dataProvider: dataProvider)
        let viewController = SchoolsListViewController(dataManager: dataManager)
        window = UIWindow(windowScene: windowScene)
        window?.rootViewController = UINavigationController(rootViewController: viewController)
        window?.makeKeyAndVisible()
    }

}

