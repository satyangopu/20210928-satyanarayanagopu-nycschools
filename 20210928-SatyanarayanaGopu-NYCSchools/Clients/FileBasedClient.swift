//
//  FileBasedClient.swift
//  20210928-SatyanarayanaGopu-NYCSchools
//
//  Created by Satya on 9/28/21.
//

import Foundation

// Constants representing the error conditions of file based data provider.
enum FileBasedDataProviderError: String, LocalizedError {
    case noJsonData = "Data not available"
    
    var errorDescription: String? {
        return self.rawValue
    }
}

// Fetches the json data from a file in the bundle. Use it if the network authentication token is not availabel.
class FileBasedDataProvider {
    var schoolsJsonData: Data?
    var SATResultsJsonData: Data?
    
    init() {
        if let schoolsFilePath = Bundle.main.path(forResource: "schools", ofType: "json"),
           let schoolsJsonData = try? Data(contentsOf: URL(fileURLWithPath: schoolsFilePath)) {
            self.schoolsJsonData = schoolsJsonData
        }
        
        if let SATResultsFilePath = Bundle.main.path(forResource: "SATResults", ofType: "json"),
           let SATResultsJsonData = try? Data(contentsOf: URL(fileURLWithPath: SATResultsFilePath)) {
            self.SATResultsJsonData = SATResultsJsonData
        }
        
    }
    
}

extension FileBasedDataProvider: DataProvider {
    
    func fetchSchools(completion: @escaping (Result<[School], Error>) -> ()) {
        
        guard let jsonData = self.schoolsJsonData else {
            completion(.failure(FileBasedDataProviderError.noJsonData))
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let schools = try JSONDecoder().decode([School].self, from: jsonData)
                completion(.success(schools))
            } catch {
                completion(.failure(error))
            }
        }
        
    }
    
    func fetchSchoolSATResult(completion: @escaping (Result<[SchoolSATResult], Error>) -> ()) {
        
        guard let jsonData = self.SATResultsJsonData else {
            completion(.failure(FileBasedDataProviderError.noJsonData))
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let SATResults = try JSONDecoder().decode([SchoolSATResult].self, from: jsonData)
                completion(.success(SATResults))
            } catch {
                completion(.failure(error))
            }
        }

    }
    
}
