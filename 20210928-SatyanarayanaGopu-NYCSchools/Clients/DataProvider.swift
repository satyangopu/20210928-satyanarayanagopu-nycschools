//
//  DataProvider.swift
//  20210928-SatyanarayanaGopu-NYCSchools
//
//  Created by Satya on 9/28/21.
//

import Foundation

// Type responsible for fetching the data from an external source.
protocol DataProvider: AnyObject {
    func fetchSchools(completion: @escaping (Result<[School], Error>) -> ())
    func fetchSchoolSATResult(completion: @escaping (Result<[SchoolSATResult], Error>) -> ())
}
