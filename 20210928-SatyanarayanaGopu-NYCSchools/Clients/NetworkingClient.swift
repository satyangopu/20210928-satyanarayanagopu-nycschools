//
//  NetworkingClient.swift
//  20210928-SatyanarayanaGopu-NYCSchools
//
//  Created by Satya on 9/28/21.
//

import Foundation

// Constants representing the error conditions of a network response
enum NetworkErrors: String, LocalizedError {
    case invalidResponseError = "Request returned an invalid response"
    case noDataError = "Reqeust returned no data"
    
    var errorDescription: String? {
        return self.rawValue
    }
}

class NetworkingClient: NSObject, URLSessionDelegate {
    let schoolsURLString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    let testResultsURLString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    let token = "<<------APP TOKEN-------->>"
    let tokenHeaderName = "X-App-Token"
    
    lazy var urlSession :URLSession = {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        return session
    }()
    
    func fetchData(url: URL, completion: @escaping (Result<Data, Error>) -> ()) {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue(token, forHTTPHeaderField: tokenHeaderName)
        
        urlSession.dataTask(with: urlRequest) { (data, response, error) in
            guard error == nil else {
                completion(.failure(error!))
                return
            }
            // check if the response is a success
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                completion(.failure(NetworkErrors.invalidResponseError))
                return
            }
            
            guard let data = data else {
                completion(.failure(NetworkErrors.noDataError))
                return
            }
            
            completion(.success(data))
        }.resume()
    }

}

extension NetworkingClient: DataProvider {
    
    func fetchSchools(completion: @escaping (Result<[School], Error>) -> ()) {
        let schoolsURL = URL(string: schoolsURLString)!
        
        fetchData(url: schoolsURL) { result in
            switch result {
            case .success(let data):
                do {
                    let schools = try JSONDecoder().decode([School].self, from: data)
                    completion(.success(schools))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func fetchSchoolSATResult(completion: @escaping (Result<[SchoolSATResult], Error>) -> ()) {
        let testResultsURL = URL(string: testResultsURLString)!
        
        fetchData(url: testResultsURL) { result in
            switch result {
            case .success(let data):
                do {
                    let SATResults = try JSONDecoder().decode([SchoolSATResult].self, from: data)
                    completion(.success(SATResults))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}
