//
//  SchoolSATResult.swift
//  20210928-SatyanarayanaGopu-NYCSchools
//
//  Created by Satya on 9/28/21.
//

import Foundation

struct SchoolSATResult: Codable {
    let id: String
    let schoolName: String
    let testTakerCount: String
    let readingAvg: String
    let mathAvg: String
    let writingAvg: String
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case testTakerCount = "num_of_sat_test_takers"
        case readingAvg = "sat_critical_reading_avg_score"
        case mathAvg = "sat_math_avg_score"
        case writingAvg = "sat_writing_avg_score"
    }
    
}
