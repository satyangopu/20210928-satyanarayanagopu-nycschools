//
//  School.swift
//  20210928-SatyanarayanaGopu-NYCSchools
//
//  Created by Satya on 9/28/21.
//

import Foundation

struct School: Codable {
    let id: String
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
    }
    
}
