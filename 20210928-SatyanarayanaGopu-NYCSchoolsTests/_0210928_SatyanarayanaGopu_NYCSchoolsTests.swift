//
//  _0210928_SatyanarayanaGopu_NYCSchoolsTests.swift
//  20210928-SatyanarayanaGopu-NYCSchoolsTests
//
//  Created by Satya on 9/28/21.
//

import XCTest
@testable import _0210928_SatyanarayanaGopu_NYCSchools

class MockDataProvider: DataProvider {
    
    struct State {
        var mockSchools: [School]?
        var mockTestResults: [SchoolSATResult]?
        var callBackDelay: Double?
        var mockError: Error!
    }
    
    var state = State()
    
    func fetchSchools(completion: @escaping (Result<[School], Error>) -> ()) {
        if let schools = state.mockSchools {
            if let delay = state.callBackDelay {
                DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + delay) {
                    completion(.success(schools))
                }
            } else {
                completion(.success(schools))
            }
        } else {
            if let delay = state.callBackDelay {
                DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + delay) {
                    completion(.failure(self.state.mockError))
                }
            } else {
                completion(.failure(self.state.mockError))
            }
        }
    }
    
    func fetchSchoolSATResult(completion: @escaping (Result<[SchoolSATResult], Error>) -> ()) {
        if let testResults = state.mockTestResults {
            if let delay = state.callBackDelay {
                DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + delay) {
                    completion(.success(testResults))
                }
            } else {
                completion(.success(testResults))
            }
        } else {
            if let delay = state.callBackDelay {
                DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + delay) {
                    completion(.failure(NetworkErrors.noDataError))
                }
            } else {
                completion(.failure(NetworkErrors.noDataError))
            }
        }
    }
    
}

extension School: Equatable {
    public static func == (lhs: School, rhs: School) -> Bool {
        lhs.id == rhs.id
    }
}

class _0210928_SatyanarayanaGopu_NYCSchoolsTests: XCTestCase {
    
    lazy var sampleSchools: [School] = {
        let school1 = School(id: "1", name: "One")
        let school2 = School(id: "2", name: "Two")
        let school3 = School(id: "3", name: "Three")
        return [school1, school2, school3]
    }()
    
    lazy var sampleTestResults: [SchoolSATResult] = {
        let result1 = SchoolSATResult(id: "1", schoolName: "One", testTakerCount: "1", readingAvg: "1", mathAvg: "1", writingAvg: "1")
        let result2 = SchoolSATResult(id: "2", schoolName: "Two", testTakerCount: "2", readingAvg: "2", mathAvg: "2", writingAvg: "2")
        let result3 = SchoolSATResult(id: "4", schoolName: "Four", testTakerCount: "4", readingAvg: "4", mathAvg: "4", writingAvg: "4")
        return [result1, result2, result3]
    }()

    func testDataManagerState() {
        let dataProvider = MockDataProvider()
        dataProvider.state.mockSchools = sampleSchools
        dataProvider.state.mockTestResults = sampleTestResults
        let dataManager = DefaultDataManager(dataProvider: dataProvider)
        for school in sampleSchools {
            XCTAssertNotNil(dataManager.school(with:school.id))
        }
        for testResult in sampleTestResults {
            XCTAssertNotNil(dataManager.testResultsOfSchool(with:testResult.id))
        }
    }
    
    func testDataManagerObserver() {
        let shouldResultData = Bool.random()
        let dataProvider = MockDataProvider()
        dataProvider.state.mockSchools = shouldResultData ? sampleSchools : nil
        dataProvider.state.mockError = shouldResultData ? nil : NetworkErrors.noDataError
        let delay: Double = 1
        dataProvider.state.callBackDelay = delay
        var dataDownloadExpectation: XCTestExpectation?
        
        var observedSchools: [School]?
        var observedError: Error?
        let observer: DataManager.SchoolDataObserver = { result in
            dataDownloadExpectation?.fulfill()
            switch result {
            case .success(let schools):
                observedSchools = schools
            case .failure(let error):
                observedError = error
            }
        }
        let dataManager = DefaultDataManager(dataProvider: dataProvider)
        dataManager.observeSchools(observer: observer)
        dataDownloadExpectation = XCTestExpectation(description: "wait for data")
        wait(for: [dataDownloadExpectation!], timeout: delay + 1)
        if shouldResultData {
            XCTAssertNotNil(observedSchools)
            XCTAssertTrue(observedSchools! == sampleSchools)
        } else {
            XCTAssertEqual(dataProvider.state.mockError.localizedDescription, observedError!.localizedDescription)
        }
        
    }
    
}
